#jinja2: lstrip_blocks: "True", trim_blocks: "True"
# keepalived.conf

# this file was created from the role: {{ role_name }}


$STATIC_INTERFACE={{ my_static_interface }}
$FLOATING_INTERFACE={{ my_floating_interface }}

global_defs {
        enable_script_security
        script_user root
        router_id {{ inventory_hostname_short }}
}

{% for vip in my_config.vips | dict2items if (vip.value.sorry_page is not defined and '*' not in vip.value.floating_ips) %}
vrrp_script chk_{{ vip.key }} {
        interval 3
        script "/bin/bash /usr/local/etc/keepalived/monit.sh {{ vip.key }}"
        fall 2
        rise 2
}

{% endfor %}

{% for vip,ip in (my_config.vips | dict2items | subelements('value.floating_ips')) if (ip != '*') %}
    {% set prio = my_keepalived_priorities[ip] %}
$VID={{ 50 + loop.index0 }}
vrrp_instance {{ vip.key }}_${VID} {
{{      "# " ~ vip.value.description if (vip.value.description is defined) }}

        virtual_router_id ${VID}

        # interface used to communique with VRRP
        interface ${STATIC_INTERFACE}
        advert_int 4

        state {{ "MASTER" if (prio == my_keepalived_priorities_max) else "BACKUP" }}
        priority {{ prio }}

        # define other nodes explicitly, this will avoid same vrrp IDs collision
        unicast_peer {
    {% for node in my_config.nodes.keys() if node != inventory_hostname_short %}
            {{ my_config.nodes[node].static_ip }}
    {% endfor %}
        }

        # use this IP to contact other nodes
        unicast_src_ip {{ my_static_ip }}

        # track this interface, if it falls, we cannot contact backends any more
        track_interface {
            ${STATIC_INTERFACE}
            ${FLOATING_INTERFACE}
        }

{%      if (vip.value.sorry_page is not defined) %}
        # see global vrrp_script
        track_script {
            chk_{{ vip.key }}
        }

{%      endif %}
        # interface holding floating IP
        virtual_ipaddress {
            {{ ip }} dev ${FLOATING_INTERFACE}
        }
{%      if vip.value.routes is defined %}

        # routes to reach back clients (if needed), note the metric is *very* important
        # so keepalived does not remove the route of other MASTER vrrp_instance when switching to backup
        virtual_routes {
            {{ vip.value.routes | join('\n') | indent(12, false) }}
        }
{%     endif %}
}

{% endfor %}

