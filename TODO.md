## TODO

* replace haproxy stats page auth with a random password and store it in vault
* nodes port should also be an array
* include vars/config_validation_template.yaml in directory vars/main/ ?
  * create a my_defaut_config ?

* add keepalived --config-test[=FILE] and haproxy -c to template validation 
  * my_validate:
    "etc/haproxy/conf.d/snet.cfg": "docker exec haproxy haproxy -c -f %s"
  * validate: "{{ my_validate[item.path] if (my_validate[item.path] is defined) }}"
  but '%s' is outside docker filestree

* manage logging (and snmp trap) on events

* group floating IPs by priority in keepalived.conf in same vrrp VI, or use vrrp_sync_group
  * use last != current && vrrp_script and vrrp_sync_group

* implement ipv6 frontends
  * with static_ip4, static_ip6, floating_ips4/6 ... or ansible.utils.ipaddr

* static_ip: should get it's value from ansible_default_ipv4.address where ansible_default_ipv4.interface == \*static_interface
  * same with ansible_default_ipv6.address
  * all_nodes_ipv4: "{{ hostvars | json_query('\*.ansible_default_ipv4.address') }}"
  * all_nodes_ipv6: "{{ hostvars | json_query('\*.ansible_default_ipv6.address') }}"

* think about a solution layer 3 with keepalived ipvs implementation

* think about ANSIBLE_DUPLICATE_YAML_DICT_KEY=error to avoid duplicate key in config

* see include and macro to simplify templating https://ttl255.com/jinja2-tutorial-part-6-include-and-import/

* put back ipaddr filter to ansible.utils.ipaddr

* fix jsonschema of ipv4/ipv6 masks

* replace debug_path with my_config.misc.debug_path
  * add my_config.misc.haproxy_docker_name my_config.misc.keepalived_docker_name ....

* have a look at blacklist_paths and tcp mode

