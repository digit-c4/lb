-- insert_colons.lua

local function add_colons(value)
  local formatted_value = value:gsub("(.)(.)", "%1%2%%3A"):gsub("%%3A$", "")
  return formatted_value
end

local function lowercase(value)
  local lowercase_value = string.lower(value)
  return lowercase_value
end

local char_to_hex = function(c)
  return string.format("%%%02X", string.byte(c))
end

local function urlencode(url)
  if url == nil then
    return
  end
  url = url:gsub("([^%w%s-.])", char_to_hex)
  url = url:gsub(" ", "%%20")
  return url
end

local function format_cert_subject(value)
  local subject = value:gsub("^/", ""):gsub("/", ", ")
  return subject
end

local function format_date(value)
  local year, month, day, hour, min, sec = value:match("(%d%d)(%d%d)(%d%d)(%d%d)(%d%d)(%d%d)")
  local months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}
  local month_name = months[tonumber(month)]
  return string.format("%s %s %s:%s:%s 20%s GMT", month_name, day, hour, min, sec, year)
end

core.register_converters("add_colons", add_colons)
core.register_converters("lowercase", lowercase)
core.register_converters("urlencode", urlencode)
core.register_converters("format_cert_subject", format_cert_subject)
core.register_converters("format_date", format_date)
