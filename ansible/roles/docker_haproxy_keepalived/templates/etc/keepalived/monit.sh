#!/bin/bash

# old test:
# /usr/bin/curl --silent --fail --output /dev/null http://localhost:9090/haproxy-health-check-${1}.htm

wget --quiet --timeout=2 --output-document=- {{ my_haproxy_stats_url }} | grep -q "^${1},.*,UP,"

