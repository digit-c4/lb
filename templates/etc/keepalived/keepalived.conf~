# keepalived.conf

# this file was created from the role: {{ role_name }}

{% for i in range(0, my_nb_hosts) %}
{% if i == my_index | int %}
# definition for node{{ i }} == this host ({{ inventory_hostname }})
$STATIC_INTERFACE={{ my_config.nodes[i].static_interface | default(my_config.nodes[0].static_interface) }}
$FLOATING_INTERFACE={{ my_config.nodes[i].floating_interface | default(my_config.nodes[0].floating_interface) }}
{% else %}
# definition for node{{ i }} ({{ ansible_play_hosts_all[i] }})
{% endif %}
$STATIC_IP{{ i+1 }}={{ my_config.nodes[i].static_ip }}
$FLOATING_IP{{ i+1 }}={{ my_config.nodes[i].floating_ip }}

{% endfor %}

global_defs {
        enable_script_security
        script_user root
}

vrrp_script chk_haproxy {
        interval 5
        # this test works with monitor-uri and monitor fail in frontend keep-ha-front
        # see /etc/haproxy/conf.d/snet.cfg
        script "/usr/bin/curl --silent --fail --output /dev/null http://localhost:8012/haproxy-health-check.htm"
        fall 2
        rise 2
}

{% for i in range(0, my_nb_hosts) %}
$VID={{ 50+i }}
vrrp_instance HAPROXY_${VID} {

        virtual_router_id ${VID}

        # interface used to communique with VRRP
        interface ${STATIC_INTERFACE}
        advert_int 4

# see man -P "less +/Conditional.configuration" keepalived.conf
{% for j in range(0, my_nb_hosts) %}
{% set index = my_nb_nodes|int - i|int + j|int %}
@host{{ j+1 }}  state {{ "MASTER" if (i == j) else "BACKUP" }}
@host{{ j+1 }}  priority {{ my_keepalived_priorities_tmp[index|int] }}
{% endfor %}

        # define other nodes explicitly, this will avoid same vrrp IDs collision
        unicast_peer {
{% for j in range(0, my_nb_hosts) %}
@^host{{ j+1 }}      ${STATIC_IP{{j+1}}}
{% endfor %}
        }

        # use this IP to contact other nodes
{% for j in range(0, my_nb_hosts) %}
@host{{ j+1 }}  unicast_src_ip     ${STATIC_IP{{j+1}}}
{% endfor %}

        # track this interface, if it falls, we cannot contact backends any more
        track_interface {
            ${STATIC_INTERFACE}
            ${FLOATING_INTERFACE}
        }

        # see global vrrp_script
        track_script {
            chk_haproxy
        }

        # interface holding floating IP
        virtual_ipaddress {
            ${FLOATING_IP{{ i+1 }}} dev ${FLOATING_INTERFACE}
        }
        
        # routes to reach back clients (if needed), note the metric is *very* important
        # so keepalived does not remove the route of other MASTER vrrp_instance when switching to backup
        virtual_routes {
{% for route in (my_config.nodes[i].routes | default(my_config.nodes[0].routes)) %}
            {{ route }}
{% endfor %}
        }
}

{% endfor %}

