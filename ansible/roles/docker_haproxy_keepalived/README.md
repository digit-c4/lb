# docker haproxy keepalived

Docker, Haproxy and Keepalived load balancer installation role.

#### Table of contents

1. [Description](#description)
2. [Requirement](#requirement)
3. [Configuration](#configuration)
4. [Limitations](#limitations)
5. [Debuging](#debugging)


## Description

This role intends to install a load balancer with [keepalived](http://keepalived.org) and [haproxy](http://haproxy.com) to replace an F5 load balancer.

Keepalived and Haproxy will run inside 2 distincts docker images.

The floating IPs are managed by keepalived and the load balancing itself is managed by haproxy.

## Requirement

You'll need at least two machines
- able to communicate with VRRP
- able to communicate with users
- able to communicate with backends
You'll also need at least two backends per VIP

## Configuration

See [defaults/README.yaml](defaults/README.yaml)

It's also possible to add your own configuration in a file in /etc/haproxy/conf.d/whatever.cfg but it's not recommended as it won't go though all checks.

## Limitations

- Udp is not supported by haproxy
- Haproxy does not support ipsec
- Ipv6 is only implemented on backends due to limitation in keepalived
  - see man --pager="less +/virtual_ipaddress_excluded" keepalived.conf
  - and https://e-mc2.net/blog/keepalived-documentation-nightmare/

## Debug

- try ansible-playbook ~/usr/share/ansible/playbooks/include-role.yaml -e role=docker_haproxy_keepalived -e host=target -e @defaults/README.yaml --skip-tags ifup,sysctl,mkdocker,check -e debug_path="/tmp/{{ inventory_hostname }}"
- ansible-playbook --tags all,debug ....
- read syslog and docker logs -f haproxy and docker logs -f keepalived
- tcpdump -pn -i any vrrp
- ...
